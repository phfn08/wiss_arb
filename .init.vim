nmap <F9> :!latexmk -pdf <cr>
let g:auto_save = 1  " enable AutoSave on Vim startup
nmap <leader>o<leader> :!xdg-open
inoremap .<space> .<cr>
" inoremap o: <c-k>o:
" inoremap a: <c-k>a:
" inoremap u: <c-k>u:
" inoremap SS <c-k>ss

" nmap <F5> :e _build<cr>

